import json 
import argparse 
orderString = ["stxs_bin","delta_75","delta_150","delta_250","delta_400","delta_1","delta_2","delta_tot","delta_tot_geneva_stxs","delta_tot_geneva_resum"]
format_row = "{:>20} " * (len(orderString)+1)
parser = argparse.ArgumentParser()
parser.add_argument('--print_all',default=False,help ='print the full table',action="store_true")
parser.add_argument('--stxs_bin', default=None, help = 'STXS bin label')
parser.add_argument('--delta', default=None,help="the uncertainty component or print total")
args = parser.parse_args()
with open('vh_stxs_unc_pwg_geneva.json') as json_file:
    data = json.load(json_file)
    if args.print_all:print(format_row.format("",*orderString))#"stxs_bin","delta_75","delta_150","delta_250","delta_400","delta_1","delta_2","delta_tot","delta_tot_geneva_stxs","delta_tot_geneva_resum \n"
    for subd in data:
        if args.print_all:
            print(format_row.format("",*[subd[i] for i in orderString]))
        elif args.stxs_bin is not None and args.delta is None:
            if subd["stxs_bin"]==args.stxs_bin:
                for i in range(len(orderString)):
                    print orderString[i],':',subd[orderString[i]]
        elif args.stxs_bin is not None and args.delta is not None:
            if subd["stxs_bin"]==args.stxs_bin:print subd["stxs_bin"], args.delta, '=' ,subd[args.delta]
