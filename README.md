# VH STXS uncertainties
To print out the table for all bins and uncertainty components use: 

`python getUnc.py --print_all`

If you want the uncertainies to be printed for individual STXS bin use:
`python getUnc.py  --stxs_bin=ZH_PTV_75_150_GE2J` or `python getUnc.py  --stxs_bin=ZH_PTV_75_150_GE2J --delta=delta_1` to print the uncertainty components separately. 

The results are presented in one of the LHC Higgs Cross Section WG 2 meetings and available at [link](https://indico.cern.ch/event/931479/contributions/3914587/attachments/2064037/3463445/STXSUncUpdate250620.pdf)

